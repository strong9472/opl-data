        United Powerlifter                                                                                                                                                                                                                           WUAP



       World Championships 2009
                    Saint Avold, France
  © Martin Heindl
R Firstname   Lastname           Country      Age Weight Devision Class Squat 1        Squat 2     Squat 3        Squat 4     Bench 1       Bench 2        Bench 3    Bench 4    Deadlift 1 Deadlift 2 Deadlift 3 Deadlift 4   Total        Reshel McCullough
  Women
1 Janette     Junge              Germany      22    56,70   Junior   60 kg      70,0     80,0     80,0              45,0     50,0    50,0                              56,0 wr    65,0     75,0     82,5                       205,0        386,84
1 Chantal     Rostagnat          France       42    59,40   M40-44 60 kg        95,0 er 100,5 wr 112,5 wr 117,5 wr 70,0 er 70,0 er 70,0 er                                       120,0 er 132,5 er 140,0 er 148,0 wr           315,0 er     567,63     575,577
1 Anikó       Strinyi            Hungary      42    79,35   M40-44 82,5 kg     180,0 wr 180,0 wr 200,0 wr 200,0 wr 90,0     100,0   108,0 wr                                     150,0    150,0    165,0                       430,0        646,29     655,338
1 Svetlana    Baker              USA          55    49,40   M55-59 52 kg       100,0 wr 100,0 wr 100,0 wr           55,0 wr 57,5 wr 60,0 wr                                      125,0 wr 135,0 wr 135,0 wr                    282,5 wr     629,98     787,469
1 Katarína    Moňáková           Czech Rep.   35    72,60   Submaster75 kg      70,0     70,0     85,0              50,0     62,5    72,5                                         80,0     95,0    110,0                       257,5        405,31
- Sandra      Mokrá              Slovakia     30    67,50   Open   67,5 kg     175,0    175,0    175,0                                                                                                                           0,0
1 Judit       Sándor             Hungary      29    70,35   Open     75 kg     170,0    185,0 wr 185,0 wr          105,0    105,0   110,0                                        190,0      200,0       205,0 wr               500,0 wr     800,50
  Teenager, Junior
1 Grzegorz    Głowacki           Poland       16  58,05     T16-17    60 kg     45,0     52,5 wr    60,0     wr           50,0     60,0 wr                  65,0 wr               75,0       90,0 wr 100,0 wr                  225,0 wr     336,83
1 Mário       Kraus              Slovakia     17  66,60     T16-17   67,5 kg   180,0    180,0      200,0                  90,0    100,0                    110,0                 190,0      200,0      0,0                     480,0        603,36
1 Tomás       Kalenský           Czech Rep.   16  74,05     T16-17    75 kg    220,0    235,0 wr   245,0     wr 250,0 wr 110,0    120,0                    130,0                 195,0      210,0    221,0 wr 227,5 wr         576,0        652,03
1 Alex        Fernandes          France       17  82,50     T16-17   82,5 kg   145,0    160,0      175,0                  75,0     82,5                     85,0                 195,0      215,0    223,5 wr                  455,0        468,20
1 Thomas      Mohs               Germany      18 117,20     T18-19   125 kg    170,0    177,5      182,5                 120,0    122,5                    127,5                 205,0      212,5    217,5                     522,5        454,05
1 Ján         Habardík           Slovakia     23  67,25     Junior   67,5 kg   240,0 wr 250,0 wr   250,0     wr 260,0 wr 135,0 wr 140,0 wr                 145,0 wr              215,0      225,0    225,0                     615,0 wr     763,83
- Kévin       Dufrenoy           France       20  82,15     Junior   82,5 kg   200,0    220,0      230,0                 112,5    120,0                    120,0                 225,0      225,0    225,0                       0,0
1 Mark        Susovits           Hungary      22  99,15     Junior   100 kg    235,0    250,0      260,0                 170,0    177,5                    182,5                 250,0      260,0    270,0 wr 275,0 wr         712,5        654,79
2 Florian     Hager              Germany      23  99,60     Junior   100 kg    255,0    270,0      275,0                 155,0    170,0                    175,0                 230,0      230,0    230,0                     680,0        623,56
3 Adam        Chapelier          France       22  98,75     Junior   100 kg    245,0    260,0      272,5                 145,0    152,5                    152,5                 230,0      230,0    232,5                     635,0        584,20
1 Ľubomír     Gréger             Slovakia     22 110,20     Junior   125 kg    300,0    320,0 wr   330,0     wr          230,0 wr 245,0 wr                   0,0                 230,0      250,0      0,0                     815,0 wr     721,28
  Master
1 Günther     Schleinzer         Austria      41    79,80   M40-44 82,5 kg 250,5 wr 265,0 wr 275,0 wr                          100,0    120,0              130,0               220,0    230,0    237,5                         642,5   er   679,12     682,518
2 Rocco       Göpfarth           Germany      44    82,30   M40-44 82,5 kg 225,0    245,0    255,0 er                          155,0    162,5              167,5               200,0    210,0    220,0                         642,5        662,42     691,564
1 Didier      Hafer              France       40    83,85   M40-44  90 kg 190,0     210,0    225,0                             142,5    147,5              155,0               220,0    232,5    240,0 er                      620,0        630,54     630,540
2 Steffen     Reisbach           Germany      41    88,95   M40-44  90 kg 210,0     220,0    225,0                             170,0    175,0              180,0               200,0    212,5    220,0                         620,0        606,36     609,392
1 László      Kupcsik            Hungary      42    93,60   M40-44 100 kg 290,0     310,0 wr   0,0                             260,0    270,0         wr   280,0   wr          260,0    275,0    280,0                         855,0   wr   807,98     819,287
1 Csaba       Berki              Hungary      40   109,50   M40-44 110 kg 260,0     290,0 wr 320,0 wr                          190,0    200,0              210,0               225,0    240,0      0,0                         725,0   er   642,35     642,350
2 Eric        Bar                France       40   110,00   M40-44 110 kg 215,0     225,0    230,0                             180,0    180,0              180,0               270,0 er 270,0 er 270,0 er 285,5 wr             675,0        597,38     597,375
1 Tom         Oberle             USA          44   119,80   M40-44 125 kg 250,0     260,0    272,5                             150,0    165,0              165,0               200,0    225,0    245,0                         682,5        589,68     615,626
2 Robert      Głowacki           Poland       42   116,25   M40-44 125 kg 240,0       0,0      0,0                             100,0      0,0                0,0               250,0    280,0    280,0                         590,0        513,30     520,486
1 Jochen      Hofstetter         Germany      42   138,10   M40-44 140 kg 220,0     250,0      0,0                             220,0    250,0         wr   260,0   wr          200,0    270,0 er 300,0 er                      810,0   er   682,02     691,568
1 František   Öszi               Slovakia     43   148,80   M40-44 140+kg 280,0     280,0    305,0 er                          160,0    170,0              170,0               270,0    302,5 er   0,0                         752,5   er   626,08     643,610
1 Michael     Molgedey           Germany      45    79,70   M45-49 82,5 kg 230,0 er 245,0 er 255,0 er                          140,0    147,5              152,5               210,0    220,0    227,5 er                      617,5   er   654,55     693,823
1 Peter       Rathke             Germany      49    88,75   M45-49  90 kg 235,0     245,0    255,0                             185,0    197,5              197,5      205,5 wr 220,0    235,0    242,5                         685,0        669,93     758,361
1 Rocky       Tilson             USA          46   114,50   M45-49 125 kg 252,5     282,5    320,0                             105,0    190,0              190,0               182,5    250,0    275,0                         637,5        556,54     599,947
2 Günther     Fuchs              Germany      49   113,60   M45-49 125 kg 170,0     190,0    200,0                             140,0    155,0              155,0               170,0    200,0    205,0                         525,0        459,90     520,607
1 Frank       Hurrass            Germany      48   130,20   M45-49 140 kg 310,0     341,0 wr 360,0 wr                          190,0    201,5         wr   210,0   wr 222,5 wr 230,0    250,0 wr 260,0 wr                      811,0   wr   690,16     768,839
1 Frank       Kutzsch            Germany      53    89,10   M50-54  90 kg 240,0     255,0    265,0                             170,0    180,0              187,5               235,0    252,5    268,0 wr                      705,0        688,08     830,513
2 Ronnie      Baker              USA          51    86,85   M50-54  90 kg 240,0     260,0    275,0 ac                          155,0    165,0         ac   182,5   ac          205,0 ac   0,0      0,0                         645,0   ac   639,84     747,333
3 Henry       Trost              Germany      50    89,15   M50-54  90 kg 190,0     200,0    210,0                             120,0    130,0              135,0               215,0    225,0    235,0                         580,0        566,08     650,992
- Joachim     Flett              Germany      51    99,60   M50-54 100 kg 260,0       0,0      0,0                             250,0 wr 250,0         wr   250,0   wr                                                            0,0
1 Burkhard    Steffen            Germany      59    70,70   M55-59  75 kg 155,0     165,0    175,0                             110,0    115,0              120,0               145,0    152,5    160,0                         455,0        539,63     728,501
1 Hans-Jürgen Kaule              Germany      55    75,80   M55-59 82,5 kg 190,0    210,0 wr 210,0 wr                          140,0    140,0              151,0   wr          190,0    205,0    215,0 wr                      566,0   wr   626,56     783,203
2 Józef       Bejgrowicz         Poland       57    82,45   M55-59 82,5 kg 140,0      0,0      0,0                              60,0      0,0                0,0               200,0    220,0 wr 227,5 wr                      427,5        440,75     571,656

        wr - worldrecord
        ac - american continental record
        er - european record                                                                                       http://united-powerlifter.eu.tf/
          United Powerlifter                                                                                                                                                                                                                           WUAP



         World Championships 2009
                      Saint Avold, France
    © Martin Heindl
R   Firstname   Lastname           Country      Age Weight    Devision   Class     Squat 1     Squat 2     Squat 3   Squat 4  Bench 1          Bench 2        Bench 3 Bench 4 Deadlift 1    Deadlift 2   Deadlift 3 Deadlift 4   Total        Reshel McCullough
1   József      Sztanke            Hungary      55   89,85    M55-59      90 kg     240,0 wr    260,0 wr    280,0 wr           190,0 wr         210,0 wr       220,0 wr         200,0        215,0        230,0      240,5 wr    710,0   wr   688,70    860,875
1   Helmut      Gedig              Germany      59   97,65    M55-59     100 kg     110,0       120,0       130,0               65,0             70,0           75,0            145,0        155,0        160,0                  360,0        333,00    449,550
1   Attila      Sándor             Hungary      57 104,60     M55-59     110 kg     175,0       190,0       210,0 wr 220,0 wr 152,5             162,5 wr       170,0 wr         165,0        185,0 er     200,0 er               580,0   wr   521,42    676,282
1   Marcel      Ingold             Swiss        62   74,70    M60-64      75 kg     145,0       155,0       165,0               90,0             97,5          105,0            150,0        160,0          0,0                  430,0        483,32    695,981
2   Joachim     Winterscheidt      Germany      62   74,30    M60-64      75 kg     110,0       120,0       130,0              105,0            110,0          115,0            140,0        150,0        165,0                  410,0        462,48    665,971
1   Frieder     Hachenberger       Germany      66   93,50    M65-69     100 kg     220,0       230,5 wr      0,0              150,0            150,0          155,5 wr         180,0        205,0 wr       0,0                  585,5   wr   553,30    865,911
1   LB          Baker              USA          72   89,90    M70-74      90 kg     150,0       165,0       175,0 wr           110,0            120,0            0,0            150,0        175,0        192,5 wr               487,5        472,88    841,718
1   Csaba       Keszthelyi         Hungary      75   74,10    M75-79      75 kg     105,0       112,5       115,5 wr            70,0             75,0           77,5 er 80,0 er 125,0        130,0        133,0 wr 135,0 wr      326,0   er   369,03    701,161
1   Günther     Tschierschky       Germany      75   82,35    M75-79     82,5 kg    155,0 wr    165,0 wr    165,0 wr            95,0 wr         100,0 wr       105,0 wr         170,0 wr     180,0 wr     185,0 wr               445,0   wr   458,80    871,711
    Open, Submaster
1   Rudolf      Siska              Slovakia     38    73,60   Submaster75 kg       200,0    215,0    222,5                        165,0    170,0              170,0                 190,0    205,0      0,0              597,5                680,55
1   Denis       Buschke            Germany      37    88,65   Submaster90 kg       245,0    245,0    260,0                        160,0    165,0              170,0                 150,0    200,0    240,0              650,0                636,35
1   Markus      Rücker             Germany      35    99,90   Submaster
                                                                      100 kg       285,0    305,0    315,0       er               180,0    192,5              202,5   er            260,0    282,5 er 292,5 wr           800,0 er             732,80
1   Norbert     Hablicsek          Hungary      35   107,55   Submaster
                                                                      110 kg       260,0    280,0    300,0                        150,0    185,0              200,0                 250,0    275,0    280,0              735,0                654,89
1   Reiko       Kruse              Germany      37   120,45   Submaster
                                                                      125 kg       400,0    425,0    437,5       wr               235,0    250,0         wr   257,5   wr            310,0    327,5 wr 327,5 wr          1015,0 wr             876,96
2   Stephen     Parkhurst          USA          35   114,05   Submaster
                                                                      125 kg       320,0    340,0    365,0       ac               210,0    227,5         ac   235,0   ac            290,0    317,5    327,5 wr           892,5 ac             780,94
1   Marcel      Mette              Germany      33    67,50   Open    67,5 kg      260,5 wr 290,0 wr 300,0       wr               140,0    150,0              150,0                 240,5 wr 250,0 wr 256,0 wr 256,0 wr 700,0 wr              865,90
1   Jeffrey     Podszuweit         Germany      32    74,05   Open     75 kg       290,0    310,0    320,0                        200,0    210,0         wr   217,5   wr   222,5 wr 250,0    265,0    277,5 er           815,0 wr             922,58
2   Paolo       Russo              Italy        33    73,30   Open     75 kg       150,0    155,0    175,0                        120,0    120,0              125,0                 220,0    232,5    240,0              535,0                611,51
-   Saverio     Manuardi           Italy        33    74,75   Open     75 kg       180,0    180,0    190,0                        110,0    110,0              110,0                                                        0,0
1   Milan       Gejdoš             Slovakia     29    80,65   Open    82,5 kg      300,0    320,0 wr 320,0       wr               200,0    210,0              215,0                 240,0    260,0    270,0              770,0                806,96
2   R.J.        Dowdell            USA          30    81,70   Open    82,5 kg      240,0    260,0 ac 260,0       ac               167,5 ac 182,5         ac   192,5   ac            232,5    252,5 ac 260,0 ac           705,0 ac             732,50
1   Patrik      Halai              Slovakia     32    89,45   Open     90 kg       290,0    315,0    325,0                        190,0    205,0              212,5                 245,0    260,0    270,0              775,0                754,85
2   Konstantin Ožinskij            Czech Rep.   24    89,15   Open     90 kg       220,0    240,0    255,0                        115,0    125,0              132,5                 220,0    230,0      0,0              600,0                585,60
1   Robert      Gyenge             Hungary      30    99,40   Open    100 kg       260,0    270,0    280,0                        200,0    207,5              212,5                 260,0    270,0    280,0              762,5                699,98
2   Sven        Lorenz             Germany      30    98,35   Open    100 kg       285,0    305,0    305,0                        180,0    200,0              200,0                 260,0    270,0    270,0              725,0                668,45
3   Andrej      Smutný             Slovakia     32    93,30   Open    100 kg       260,0    280,0    280,0                        200,0    200,0              207,5                 230,0    242,5    257,5              710,0                672,37
4   Vlodzimierz Hess               Slovakia     26    97,85   Open    100 kg       260,0    260,0    260,0                        175,0    182,5              182,5                 220,0    235,0    235,0              677,5                626,01
1   Andriy      Yaremus            Ukraine      32   109,55   Open    110 kg       320,0    335,0    345,0                        200,0    220,0              230,0                 280,0    310,0    327,5 wr           885,0                784,11
2   Jozef       Svitok             Slovakia     24   109,10   Open    110 kg       310,0    310,0    330,0                        210,0    220,0              230,0                 260,0    280,0    280,0              790,0                700,73
-   Alexander   Mittag             Germany      26   107,80   Open    110 kg       320,0      0,0                                                                                                                          0,0
1   Róbert      Németh             Hungary      34   123,80   Open    125 kg       320,0    350,0    350,0                        180,0         200,0         210,0                 240,0    270,0    300,0              830,0                712,97
2   Pavel       Stoklasa           Czech Rep.   29   112,00   Open    125 kg       240,0    260,0    280,0                        165,0         180,0         187,5                 200,0    220,0    220,0              660,0                580,14
-   Ingo        Kunstmann          Germany      34   124,25   Open    125 kg       360,0    375,0    375,0                                                                                                                 0,0
1   Serhiy      Oleolenko          Ukraine      31   138,55   Open    140 kg       380,0    400,0      0,0                        250,0         260,0         270,0                 330,0 wr 370,0 wr 400,0 wr          1030,0                867,26
2   Holger      Kuttroff           Germany      40   135,00   Open    140 kg       300,0    330,0    350,0                        200,0         225,0         235,0                 300,0    330,0 wr 340,0 wr           915,0                774,09     774,090
1   André       Steinert           Germany      26   143,80   Open   140+kg        320,0    345,0    360,0                        180,0         200,0         210,0                 240,0    255,0    265,0              835,0                698,90




          wr - worldrecord
          ac - american continental record
          er - european record                                                                                        http://united-powerlifter.eu.tf/
